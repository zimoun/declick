export default {
  domain: 'localhost',
  basePath: '/declick-ui/dist/',
  apiUrl: 'http://localhost:8000/api/v1/',
  clientUrl: 'http://declick-client.localhost/',
  cmsUrl: 'http://v2.declick.net/cms/',
  forumUrl: 'http://test.declick.net/forum',
  wikiUrl: 'http://create.declick.net/wiki/',
  seoId: 'UA-XXXXXXX-X',
}
